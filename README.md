### nginx-content-sidecar ###

* The nginx-content-sidecar demonstrates a sidecar container pattern to update content in an NGINX container.
* 1.0

### Contents ###

* nginx-template.json - Sample OpenShift Template that demonstrates how to deploy the POD
* nginx-container - Dockerfile and supporting files for the NGINX Container
* content-container - Dockerfile and supporting files for the NGINX Content Container

### How it Works ###

The nginx-template builds a POD that creates an EmptyDir volume that is shared by both containers.

This volume is mounted at /usr/share/nginx/html

The NGINX configuration file is configured to present its content from this folder.

The content container adds a folder called content to its container on build.

ADD content /content

The run command of the container copies the files from the /content folder in the content container to the EmptyDir Mounted volume:

CMD [ "sh", "-c", "cp -fr /content/* /usr/share/nginx/html ; tail -f /dev/null" ]

It then does a tail -f /dev/null to keep the container running.  This is necessary until OpenShift 3.5 when init containers are supported and this container can be changed to an init container.


### Running the Sample ###
To test, you can deploy an application based on the provided template.

oc new-app --file=nginx-template.json -p APP_NAME=test-sidecar

You can then navigate to the NGINX server using the route DNS name of the deployed application.

http://test-sidecar-sidecar.127.0.0.1.xip.io/

To test the content update, you can update the index.html file in this git repository.

nginx-content-sidecar/content-container/content/index.html

Then start a the build configuration of the content container.   This will buid a new content container and redeploy the POD.  The new content of index.html will be presented if you load the NGIX URL again.