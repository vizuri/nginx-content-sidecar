oc delete bc test-sidecar-nginx
oc delete bc test-sidecar-nginx-content
oc delete dc test-sidecar
oc delete svc test-sidecar
oc delete route test-sidecar
oc delete is test-sidecar_nginx
oc delete is test-sidecar_content
